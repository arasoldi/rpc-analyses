# Analyses on the Registre de Preuves de Covoiturage

> Warning: this repository is a preliminary work in my PhD Thesis to evaluate the carpooling potential.
Hence, it is a work in progress.
It is likely that running this project is not straightforward. If you encounter an issue, please contact me.

## Prerequisites

## Conda

[Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/) [documentation](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)


### Install the environment

```
conda env create -f environment.yml
conda activate transportcompare
```

### Update the environnement

```
conda env update -f environment.yml
```

## GraphHopper

Use [my fork of GraphHopper](https://gitlab.inria.fr/these-aina/graphhopper).

Fill it with OSM Data of the relevant zone in France. (For example, [Rhône-Alpes](https://download.geofabrik.de/europe/france/rhone-alpes-latest.osm.pbf) region.)

### Public transport data

Public transport data come from [Point d'Accès National](https://transport.data.gouv.fr/).

The tools to (1) download all the files, (2) filter them and (3) keep only the valide ones are in the folder `pan_tools`.
After executing all the scripts with the desired parameters, you get a list of valid GTFS files that you can write in the configuration of GraphHopper (see the official example [here](https://github.com/graphhopper/graphhopper/tree/master/reader-gtfs)).
You can put several GTFS files by separating them with commas.

## Usage

### 1. Convert the RPC in mobility needs

> Prerequire the data from the [Registre de Preuves de Covoiturage](https://www.data.gouv.fr/fr/datasets/trajets-realises-en-covoiturage-registre-de-preuve-de-covoiturage/).

```
python -m mobfw.rpc.convert [PATH_TO_RPC] [PATH_TO_MOBILITY_NEEDS]
```

### 2. Filter the mobility needs

```
python -m mobfw.filter
```

### 3. Generate itineraries from GraphHopper

> Prerequire an running instance of [GraphHopper](https://gitlab.inria.fr/these-aina/graphhopper).

```
python -m mobfw.compute_itineraries [PATH_TO_NEEDS] [OUTPUT_PATH]
python -m mobfw.compute_transit_itineraries [PATH_TO_NEEDS] [OUTPUT_PATH]
python -m mobfw.rpc.compute_ridesharing_itineraries [PATH_TO_NEEDS] [OUTPUT_PATH]
```

### 4. Filter API responses

```
python -m mobfw.extract [PATH_TO_ITINERARIES] [OUTPUT_PATH]
```

### 5. Generate the visualisation from the Jupyter notebooks

Launch Jupyter Labs in the snippet subfolder (in a terminal: `jupyter lab`).

Change the data paths where needed.
