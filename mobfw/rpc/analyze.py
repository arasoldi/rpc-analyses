"""
Analyze RPC
"""

import sys
from datetime import datetime
import pandas as pd
from mobfw.graphhopper.compute_transit_itineraries import on_day


def to_dt(col):
    '''Apply `on_day` on a full column'''
    return pd.to_datetime(col.apply(on_day), utc=True)


def ratio_and_diff_transit_car(journey):
    '''Compare driving and transit travel times with ratio and difference'''
    if journey[('transit', 'time')] is not None and journey[('transit', 'time')]:
        return (journey[('car', 'time')] / journey[('transit', 'time')],
                journey[('transit', 'time')] - journey[('car', 'time')])
    return (None, None)


def compare_transit_driving(rpc):
    '''Compare transit departure and arrival time and travel time with car itineraries.'''

    can_transit = \
        ~rpc['journey_start_daytime'].isna() & ~rpc[('transit', 'departure_daytime')].isna() \
        & ~rpc['journey_end_daytime'].isna() & ~rpc[('transit', 'arrival_daytime')].isna()

    rpc_transit = rpc[can_transit]

    rpc['diff_departure_time'] = \
        to_dt(rpc_transit['journey_start_daytime']) - \
        to_dt(rpc_transit[('transit', 'departure_daytime')])

    rpc['diff_arrival_time'] = \
        to_dt(rpc_transit['journey_end_daytime']) - \
        to_dt(rpc_transit[('transit', 'arrival_daytime')])

    rpc['ratio_transit_driving'], rpc['diff_transit_driving'] = zip(
        *rpc.apply(ratio_and_diff_transit_car, axis=1))

    return rpc


def main():
    '''Main'''
    if len(sys.argv) < 3:
        print("Usage: python -m mobfw.rpc.analyze $PATH_OUT/rpc_filtered.pkl," +
              "$PATH_OUT/distances_beelines.pkl,$PATH_OUT/infos_itineraries.pkl," +
              "$PATH_OUT/infos_itineraries_transit.pkl $PATH_OUT/analyzed_rpc.pkl" +
              "[compare transit and driving: 1 (default: 0)]")
        sys.exit()

    filenames = sys.argv[1].split(',')
    print(datetime.now(), 'Lecture des fichiers', filenames)
    rpc_split = [pd.read_pickle(f) for f in filenames]
    print(datetime.now(), 'Concaténation des fichiers')
    rpc = pd.concat(rpc_split, axis=1)
    print(datetime.now(), "Fin de concaténation. Nombre d'enregistrements", rpc.shape[0])

    if len(sys.argv) > 3 and sys.argv[3] == '1':
        print(datetime.now(), "Comparaison TC et voiture")
        rpc = compare_transit_driving(rpc)


    print(datetime.now(), "Début d'export vers", sys.argv[2])
    rpc.to_pickle(sys.argv[2])
    print(datetime.now(), "Fin d'export")


if __name__ == '__main__':
    main()
