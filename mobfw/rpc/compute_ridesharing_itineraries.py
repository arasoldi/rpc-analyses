'''
Calcule les itinéraires en covoiturage du RPC en utilisant GraphHopper
'''

import argparse
import asyncio
from dataclasses import dataclass
import aiohttp
import numpy as np
import pandas as pd
from tqdm import tqdm

from mobfw.graphhopper.compute_itineraries import OD_COL_NAMES
from ..constants import Mode
from ..graphhopper.api import get_itinerary


def branch_next_wp(wp_path, to_visit, permutations, all_coord, incomplete):
    '''
    Recursive function which create all possible paths to pick up and drop off passengers in a
    realistic order.
    '''
    if not to_visit:
        coord_path = [wp.coord for wp in wp_path]
        if sorted(all_coord) == sorted(coord_path):
            permutations.append(coord_path)
        else:
            incomplete.append(coord_path)

    for waypoint in to_visit:
        subpath = wp_path[:]
        sub_to_visit = to_visit[:]
        subpath.append(waypoint)
        if waypoint.after is not None:
            sub_to_visit.extend(waypoint.after)
        sub_to_visit.remove(waypoint)
        branch_next_wp(subpath, sub_to_visit, permutations, all_coord, incomplete)


def waypoint_permutations(legs):
    '''
    Return all possibles waypoint permutations for a path in a ridesharing trip
    '''
    @dataclass
    class Waypoint:
        '''
        Represent a waypoint, like a chained list
        '''
        coord: tuple
        after = None

        def set_after(self, wp_after):
            '''
            Define a waypoint which comes after the current point
            '''
            if self.after is None:
                self.after = []
            self.after.append(wp_after)

        def __hash__(self):
            return hash(self.coord)

    starts_coords = []
    ends_coords = []
    starts = []
    for leg in legs:
        pt1 = (leg[0], leg[1])
        pt2 = (leg[2], leg[3])
        wp1 = Waypoint(pt1)
        wp2 = Waypoint(pt2)
        wp1.set_after(wp2)
        starts_coords.append(pt1)
        ends_coords.append(pt2)
        starts.append(wp1)

    all_coord = starts_coords + ends_coords
    permutations = []
    incomplete = []
    branch_next_wp([], starts, permutations, all_coord, incomplete)
    if not permutations:
        print(incomplete)
        raise Exception("No path found")
    return permutations


async def compute_shortest_path(legs, cache, session, semaphore):
    '''
    Search for the shortest path in a list of path
    '''
    all_path_permutations = waypoint_permutations(legs)
    min_d = np.inf
    shortest_path = None
    for path in all_path_permutations:
        duration = 0
        for wp_id in range(len(path)-1):
            wp1 = path[wp_id]
            wp2 = path[wp_id+1]
            if wp1 != wp2:
                try:
                    itinerary = cache[(*wp1, *wp2)]
                except KeyError:
                    itinerary = await get_itinerary(*wp1, *wp2, Mode.CAR, session, semaphore)
                    cache[(*wp1, *wp2)] = itinerary
                duration += itinerary['time']
        if duration < min_d:
            min_d = duration
            shortest_path = path

    if shortest_path is None:
        raise Exception("No path found")
    return shortest_path


def extract_ridesharing_journey(journey, shortest_paths, cache):
    '''
    Put legs of driving itineraries in row to create the ridesharing path of one passenger
    '''
    start_pt = (journey['start_lon'], journey['start_lat'])
    end_pt = (journey['end_lon'], journey['end_lat'])

    if start_pt == end_pt:
        return 0, 0

    shortest_path_for_trip = shortest_paths[journey['trip_id']]
    start_pt_index = shortest_path_for_trip.index(start_pt)
    end_pt_index = shortest_path_for_trip.index(end_pt, start_pt_index)
    waypoints_for_journey = shortest_path_for_trip[start_pt_index:end_pt_index+1]

    distance = 0
    time = 0
    for i in range(len(waypoints_for_journey)-1):
        if waypoints_for_journey[i] == waypoints_for_journey[i+1]:
            continue
        itinerary_leg = cache[(*waypoints_for_journey[i], *waypoints_for_journey[i+1])]
        distance += itinerary_leg['distance']
        time += itinerary_leg['time']

    assert distance > 0 and time > 0

    return distance, time


async def extract_shortest_paths(shortest_paths: dict, cache: dict) -> pd.DataFrame:
    '''
    Put legs of driving itineraries in row to create the path of one car
    Also put in cache the hypothetical direct itinerary of the driver
    '''
    trip_dist_time = {}
    for trip_id, path in shortest_paths.items():
        distance = 0
        time = 0
        for i in range(len(path)-1):
            if path[i] == path[i+1]:
                continue
            itinerary_leg = cache[(*path[i], *path[i+1])]
            distance += itinerary_leg['distance']
            time += itinerary_leg['time']

        trip_dist_time[trip_id] = pd.Series(
            {"distance": distance, "time": time})

    trip_dist_time = pd.DataFrame(trip_dist_time).T
    trip_dist_time['time'] = pd.TimedeltaIndex(
        trip_dist_time['time'], unit="ms")

    return trip_dist_time


async def compute_ridesharing_itineraries(rpc, cache):
    '''
    Calcule les itinéraires en covoiturage avec les détours et tout
    (attention : pas les détours avant premier arrêt et après le dernier arrêt)
    '''
    if cache is None:
        cache = {}
    async with aiohttp.ClientSession() as session:
        semaphore = asyncio.Semaphore(4)
        shortest_paths = {}
        trip_ids = rpc.value_counts('trip_id').index
        for trip_id in tqdm(trip_ids, total=len(trip_ids)):
            #tqdm.write("trip for " + str(trip_id))
            legs = rpc[rpc['trip_id'] ==
                       trip_id][OD_COL_NAMES].value_counts().index
            if legs.shape[0] == 1:  # S'il n'y a qu'un seul couple origine-destination
                shortest_paths[trip_id] = [
                    (legs[0][0], legs[0][1]), (legs[0][2], legs[0][3])]
                if tuple(legs[0]) not in cache:
                    cache[tuple(legs[0])] = await get_itinerary(
                        *legs[0], Mode.CAR, session, semaphore)
            elif legs.shape[0] == 2 and ((legs[0][0], legs[0][1]) == (legs[1][2], legs[1][3])) \
                    and ((legs[0][2], legs[0][3]) == (legs[1][0], legs[1][1])):
                rpc.loc[(rpc['end_lon'] == legs[1][2]) & (rpc['end_lat'] == legs[1][3])
                        & (rpc['trip_id'] == trip_id), 'trip_id'] = trip_id + 'b'
                shortest_paths[trip_id] = [
                    (legs[0][0], legs[0][1]), (legs[0][2], legs[0][3])]
                shortest_paths[trip_id +
                               'b'] = [(legs[0][2], legs[0][3]), (legs[0][0], legs[0][1])]
                outward = (legs[0][0], legs[0][1], legs[0][2], legs[0][3])
                inward = (legs[0][2], legs[0][3], legs[0][0], legs[0][1])
                if outward not in cache:
                    cache[outward] = await get_itinerary(*outward, Mode.CAR, session, semaphore)
                if inward not in cache:
                    cache[inward] = await get_itinerary(*inward, Mode.CAR, session, semaphore)
            else:
                shortest_paths[trip_id] = await compute_shortest_path(
                    legs, cache, session, semaphore)
            #tqdm.write("end for " + str(trip_id))

    journey_dist_time = [
        extract_ridesharing_journey(journey, shortest_paths, cache)
        for _jid, journey in rpc.iterrows()]

    trip_dist_time = await extract_shortest_paths(shortest_paths, cache)

    journey_dist_time = pd.DataFrame(
        journey_dist_time, index=rpc.index,
        columns=['distance_' + Mode.RIDESHARING.value, 'time_' + Mode.RIDESHARING.value])
    journey_dist_time['time_' + Mode.RIDESHARING.value] = pd.TimedeltaIndex(
        journey_dist_time['time_' + Mode.RIDESHARING.value], unit="ms")
    return journey_dist_time, trip_dist_time, cache, shortest_paths


def get_needs(rpc: pd.DataFrame, shortest_paths: dict):
    """Calculate distance and time for autosolism, and return DataFrame with extra records
    for drivers"""
    rpc = rpc.loc[:, [
        *OD_COL_NAMES, "trip_id", "passenger_seats",
        "start_datetime", "start_datetime_tz", "end_datetime", "end_datetime_tz",
        "start_insee", "end_insee"]]
    rpc.loc[:, 'driver'] = False

    def get_driver_od(path):
        return pd.Series([*path[0], *path[-1]], index=OD_COL_NAMES)

    drivers_ods = pd.Series(shortest_paths).apply(get_driver_od)
    drivers_ods.rename_axis('trip_id', inplace=True)
    drivers_ods = drivers_ods.reset_index()[[*OD_COL_NAMES, "trip_id"]]

    def get_driver_dt(journey):
        journeys_of_trip = rpc[rpc["trip_id"] == journey["trip_id"]]
        first_journey = journeys_of_trip.loc[journeys_of_trip["start_datetime"].idxmin(
        )]
        last_journey = journeys_of_trip.loc[journeys_of_trip["end_datetime"].idxmax(
        )]
        return pd.Series({
            "start_datetime": first_journey["start_datetime"],
            "start_datetime_tz": first_journey["start_datetime_tz"],
            "start_insee": first_journey["start_insee"],
            "end_datetime": last_journey["end_datetime"],
            "end_datetime_tz": last_journey["end_datetime_tz"],
            "end_insee": first_journey["end_insee"],
        }, dtype=object)

    drivers_dt = drivers_ods.apply(get_driver_dt, axis=1)

    drivers_journeys = pd.merge(
        drivers_ods, drivers_dt, left_index=True, right_index=True)
    drivers_journeys.loc[:, "passenger_seats"] = 1
    drivers_journeys.loc[:, "driver"] = True

    rpc = pd.concat([rpc, drivers_journeys])
    return rpc


async def main():
    '''Main'''
    parser = argparse.ArgumentParser(
        "Filtre des données concernant les besoins en mobilité.")
    parser.add_argument('path_in', help="Chemin vers pickle RPC")
    parser.add_argument('path_out', help="Chemin vers fichiers de sortie")
    parser.add_argument('-cache', help="fichier d'itinéraires en voiture s'il existe déjà")

    args = parser.parse_args()

    rpc_path = args.path_in
    output_path = args.path_out

    print("Reading", rpc_path)
    mobility_needs = pd.read_pickle(rpc_path)

    car_itineraries = None
    if args.cache:
        print("Converting cache")
        car_itineraries = pd.read_pickle(args.cache)
        car_itineraries.columns = ["distance", "time"]
        car_itineraries["time"] = car_itineraries['time'].dt.total_seconds() * \
            1000
        car_itineraries = car_itineraries.to_dict(orient="index")

    print("Computing ridesharing itineraries")
    journey_dist_time, trip_dist_time, car_itineraries, shortest_paths = \
        await compute_ridesharing_itineraries(mobility_needs, car_itineraries)

    car_itineraries = pd.DataFrame(car_itineraries).T
    car_itineraries.rename(
        columns={"distance": "distance_car", "time": "time_car"}, inplace=True)
    car_itineraries["time_car"] = pd.TimedeltaIndex(
        car_itineraries["time_car"], unit="ms")
    car_itineraries.to_pickle(output_path + ".car")

    needs = get_needs(mobility_needs, shortest_paths)
    journey_dist_time.to_pickle(output_path)
    trip_dist_time.to_pickle(output_path + ".trips")
    needs.to_pickle(output_path + ".needs")


if __name__ == '__main__':
    asyncio.run(main())
