"""Filtre le RPC en enlevant les trajets hors France Métropolitaine

Pour ne retenir que les trajets en Isère en octobre 2021, lancer en script :

rpc = pd.read_pickle(RPC_PATH)
rpc_202110 = rpc[(rpc['start_datetime'].dt.month == 10) & (rpc['start_datetime'].dt.year == 2021) & (rpc['end_datetime'].dt.month == 10) & (rpc['end_datetime'].dt.year == 2021)]
rpc_202110[(rpc_202110['start_department'] == '38') & (rpc_202110['end_department'] == '38')].to_pickle(OUT_PATH)
"""

import sys

import pandas as pd


def main(args):
    """Main"""
    input_path = args[1]
    output_path = args[2]

    print("Lecture de", input_path)
    rpc = pd.read_pickle(input_path)
    rpc_length = rpc.shape[0]

    print(rpc_length, "enregistrements")

    print("Suppression des trajets Outre-Mer")
    rpc = rpc[(rpc['start_department'] != '97')
              & (rpc['end_department'] != '97')]
    print("Enregistrements supprimés :", rpc_length - rpc.shape[0])
    rpc_length = rpc.shape[0]

    print("Suppression des trajets à l'étranger")
    rpc = rpc[(rpc['start_country'] == 'France')
              & (rpc['end_country'] == 'France')]
    print("Enregistrements supprimés :", rpc_length - rpc.shape[0])

    print("Nouveau nombre d'enregistrement :", rpc.shape[0])

    print("Écriture du fichier", output_path)
    rpc.to_pickle(output_path)


if __name__ == '__main__':
    main(sys.argv)
