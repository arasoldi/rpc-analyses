'''
Convert CSV RPC files to mobility needs (travel from point A to point B at defined time).
'''

from os.path import join, isfile
from os import listdir
import sys

import pandas as pd

from mobfw.utils import to_daytime

def series_to_daytime(series):
    '''Convert a Series of datetime to daytime'''
    return series.apply(lambda l: pd.Series([(to_daytime(l))]))


def read_rpc(path):
    '''Lit les fichiers originaux .csv du RPC'''

    dtypes = {
        'journey_start_postalcode': str,
        'journey_end_postalcode': str,
        'journey_start_insee': str,
        'journey_end_insee': str,
        'journey_start_department': str,
        'journey_end_department': str,
    }

    parse_dates = {
        'start_datetime': ['journey_start_date', 'journey_start_time'],
        'end_datetime': ['journey_end_date', 'journey_end_time'],
    }

    file_paths = [
        join(path, f) for f in listdir(path)
        if isfile(join(path, f)) and f.endswith('.csv')]

    rpc_month_list = []
    for file_path in file_paths:
        print('Lecture de', file_path)
        rpc_month: pd.DataFrame = pd.read_csv(
            file_path,
            delimiter=";",
            index_col='journey_id',
            dtype=dtypes,
            parse_dates=parse_dates)
        rpc_month_list.append(rpc_month)

    rpc = pd.concat(rpc_month_list)
    return rpc


def convert_rpc(rpc: pd.DataFrame):
    '''Renomme les colonnes en noms plus simples, corrige le type de journey_duration et
    calcule les jours de la semaine et l'heure de l'arrivée de chaque trajet.'''
    columns = {
        name: name[8:]
        for name in rpc.columns
        if name.startswith('journey_')
    }

    columns["journey_start_datetime"] = "start_datetime_tz"
    columns["journey_end_datetime"] = "end_datetime_tz"

    rpc = rpc.rename(columns=columns)

    rpc['duration'] = pd.TimedeltaIndex(rpc['duration'], unit='m')
    rpc['end_dayofweek'] = rpc['end_datetime'].dt.dayofweek
    rpc['end_time'] = rpc['end_datetime'].dt.time

    return rpc


def main(args):
    '''
    Main
    '''
    input_path = args[1]
    output_path = args[2]
    rpc = read_rpc(input_path)
    print("Conversion")
    rpc = convert_rpc(rpc)
    print("Export")
    rpc.to_pickle(output_path)


if __name__ == '__main__':
    main(sys.argv)
