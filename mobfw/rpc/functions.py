'''
Fonctions utilitaires pour calculer certains résultats de l'analyse du RPC
'''

import numpy as np
import pandas as pd

import geojson
from scipy.spatial import KDTree
from ..beeline.calculate_distances import geodesic_distance


def get_passenger_stats(rpc):
    '''
    Return statistics on passengers of the subset of RPC:
    - an array with the number of trips per number of passengers
    - the number of trips with 2 passengers
    - the number of trips with more than 2 passengers
    '''
    passengers_counts = rpc['trip_id'].value_counts().value_counts(normalize=True)
    rpc_pcount = rpc.merge(rpc['trip_id'].value_counts().rename('count'),
                           left_on='trip_id', right_index=True)
    rpc_1p = rpc_pcount[rpc_pcount['count'] == 1]
    rpc_np = rpc_pcount[rpc_pcount['count'] > 1]
    return passengers_counts, rpc_1p, rpc_np


def closest_stop_distance(rpc, stops):
    '''
    Compute sum of distance to closest stop at departure and arrival for each journey in sub_rpc.
    stops_data is the geojson data of the stops loaded from a file
    '''
    pts_stats = pd.concat([
        rpc['start_pt'].value_counts().rename('start_count'),
        rpc['end_pt'].value_counts().rename('end_count')
    ], axis=1, join='outer').fillna(0)

    stop_coords = np.array(list(geojson.utils.coords(stops)))
    ridesharing_pts = np.array(list(pts_stats.index))
    indexes = KDTree(stop_coords).query(ridesharing_pts, k=1)[1]
    distances_to_stop = []
    for i_pt, ridesharing_pt in enumerate(ridesharing_pts):
        distances_to_stop.append(geodesic_distance(
            ridesharing_pt, stop_coords[indexes[i_pt]]))

    pts_stats['closest_stop_distance'] = distances_to_stop

    def sum_stop_distances(journey, pts_stats):
        '''
        Sum of distances to walk to the stop at the start and end for one trip.
        '''
        d_start = pts_stats.loc[[journey['start_pt']]]['closest_stop_distance']
        d_end = pts_stats.loc[[journey['end_pt']]]['closest_stop_distance']
        return float(d_start) + float(d_end)

    return rpc.apply(
        lambda row: sum_stop_distances(row, pts_stats), axis=1)
