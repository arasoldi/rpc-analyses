'''
Functions to vizualise RPC
'''

from datetime import datetime
import sys

import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np
from cartopy.io.img_tiles import OSM
import pandas as pd
from mobfw.graphhopper.compute_itineraries import OD_COL_NAMES

from mobfw.rpc.functions import get_passenger_stats


def pass_exceptions(func):
    '''
    Function decorator to pass exceptions
    '''
    def wrapper(*args, **kwargs):
        func(*args, **kwargs)
        # try:
        #     func(*args, **kwargs)
        # except Exception as error:
        #     print(error)
    return wrapper


def plot_cumulative_density(axis, data, title, xlabel, ylabel, xstep, xmax=0):
    '''
    Plot cumulative density of data
    '''
    if xmax == 0:
        xmax = data.max()
    full_pdf = data.value_counts(sort=False).sort_index()
    pdf = full_pdf.loc[full_pdf.index <= xmax]
    axis.margins(x=0, y=0)
    axis.plot(pdf.index, np.cumsum((pdf / pdf.sum()).to_numpy()))
    axis.set_xticks(np.arange(0, xmax + xstep, xstep))
    axis.set_yticks(np.arange(0, 1.1, .1))
    axis.set_yticklabels(map(lambda label: str(
        label) + '%', np.arange(0, 101, 10)))
    axis.set_title(title)
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)
    axis.grid()


def plot_hist(axis, data, title, xlabel, ylabel, xstep=1, ystep=.1, xmax=0):
    '''
    Plot histogram of data
    '''
    ymax = data.shape[0]
    if xmax == 0:
        xmax = data.max()
    bins = np.arange(0, xmax + xstep, xstep)
    axis.yaxis.set_major_formatter(ticker.PercentFormatter(ymax))
    axis.margins(x=0, y=0)
    axis.grid(axis='y')
    axis.hist(data, bins=bins)
    axis.xaxis.set_ticks(bins)
    axis.yaxis.set_ticks(np.arange(0, axis.get_ylim()[1], ystep * ymax))
    axis.set_title(title)
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)


def set_map(axis, bbox, scale):
    '''
    Set figure and axes to show a map
    '''
    imagery = OSM()
    axis.add_image(imagery, scale)
    axis.set_extent(bbox)
    return axis


@pass_exceptions
def plot_journeys_on_map(journeys, axis=None, scale=10, bounds=None, log=False,
                         subplot_args=None, subplot_kwargs=None, latex=False):
    '''
    Plot journeys on a map
    Journeys can be filtered with `rpc_filter`
    '''
    if log:
        print(datetime.now(), "rpc.value_counts(OD_COL_NAMES)")
    start_end_counts = journeys.value_counts(OD_COL_NAMES)
    if log:
        print(datetime.now(), "rpc.value_counts(['start_pt'])")
    start_counts = journeys.value_counts(['start_lon', 'start_lat'])
    if log:
        print(datetime.now(), "rpc.value_counts(['end_pt'])")
    end_counts = journeys.value_counts(['end_lon', 'end_lat'])
    if log:
        print(datetime.now(), "np.concatenate((start_counts, end_counts))")

    starts_ends_concat = pd.concat([start_counts, end_counts])
    lons, lats = np.array(list(starts_ends_concat.index)).T

    if bounds is None:
        bounds = [min(lons) - .05, max(lons) + .05,
                  min(lats) - .05, max(lats) + .05]

    if subplot_kwargs is None:
        subplot_kwargs = {}
    if axis is None:
        axis = plt.subplot(
            *subplot_args, projection=OSM().crs, **subplot_kwargs)

    axis_map = set_map(axis, bounds, scale)

    for origin_destination, count in start_end_counts.iteritems():
        start_lon, start_lat, end_lon, end_lat = origin_destination
        axis_map.plot(
            [start_lon, end_lon], [start_lat, end_lat],
            linewidth=count / start_end_counts.max() * 10,
            transform=ccrs.PlateCarree(), c='black', alpha=0.3, zorder=1)

    handles, _ = axis_map.scatter(
        lons, lats,
        transform=ccrs.PlateCarree(),
        c=['blue' for _ in range(len(start_counts))] +
        ['red' for _ in range(len(end_counts))],
        s=starts_ends_concat/starts_ends_concat.max() * 800, alpha=.5, edgecolors='none', zorder=2
    ).legend_elements(prop="sizes", alpha=0.6)

    if latex:
        axis_map.legend(
            handles, [10, 100], loc="upper right",
            title="\# O/D")
    else:
        axis_map.legend(
            handles, [10, 100], loc="upper right",
            title="# O/D")


def main():
    """Main"""
    try:
        rpc = pd.read_pickle(sys.argv[1])
    except IndexError:
        print("Usage: ")

    rpc['distance'] /= 1000
    rpc[('car', 'distance')] /= 1000
    rpc[('car', 'time')] /= 1000
    rpc[('transit', 'distance')] /= 1000
    rpc[('transit', 'time')] /= 1000
    rpc['diff_transit_driving'] /= 1000

    _, axes = plt.subplots(1, 2, subplot_kw={'projection': OSM().crs})
    plot_journeys_on_map(axes[0], rpc)
    plot_journeys_on_map(axes[1], rpc, rpc['distance'] > 40)

    passengers_counts, rpc_1p, rpc_np = get_passenger_stats(rpc)

    # Statistiques de base
    print("Nombre d'enregistrements", rpc.shape[0])
    print("Distance moyenne de la course à vol d'oiseau (km)",
          rpc['distance'].mean())
    print("Distance moyenne de la course en voiture (km)",
          rpc[('car', 'distance')].mean())
    print("Durée moyenne de la course en voiture (min)",
          rpc[('car', 'time')].mean()/60)
    print("Nombre de passagers moyens par parcours",
          rpc.value_counts('trip_id').mean() + 1)
    print("Part du nombre de parcours avec 1 passager", passengers_counts[1])
    print("Distance géodésique correspondante (m)",
          rpc_np['distance'].sum())
    print("Distance géodésique totale (m)", rpc['distance'].sum())
    print("Part des distances parcourues avec >1 passagers", rpc_np['distance'].sum()
          / rpc['distance'].sum())
    print("Distance moyenne parcourue avec 1 passager (km)",
          rpc_1p['distance'].mean())
    print("Distance moyenne parcourue avec >1 passagers (km)",
          rpc_np['distance'].mean())

    # Distribution des distances de trajet (vol d'oiseau)
    _, axes = plt.subplots(2, 1)
    plot_hist(axes[0],
              rpc["distance"],
              "Distribution des distances à vol d'oiseau des courses",
              "Distance (km)", "Proportion", 10)

    plot_cumulative_density(axes[1],
                            rpc["distance"],
                            "Cumul des distances à vol d'oiseau des courses",
                            "Distance (km)", "Proportion", 10)

    # Distance parcourue en voiture
    _, axes = plt.subplots(2, 1)
    plot_hist(axes[0],
              rpc[('car', 'distance')],
              "Distribution de la distance parcourue en voiture",
              "Distance parcourue en voiture (km)",
              "Proportion des courses", 10)

    plot_cumulative_density(axes[1],
                            rpc[('car', 'distance')],
                            "Cumul des courses en fonction de la distance parcourue",
                            "Distance parcourue en voiture (km)",
                            "Proportion des courses", 10)

    # Durée de course
    _, axes = plt.subplots(2, 1)
    plot_hist(axes[0],
              rpc[('car', 'time')] / 60,
              "Distribution de la durée d'une course en voiture",
              "Durée parcourue en voiture (min)",
              "Proportion des courses", 10)

    plot_cumulative_density(axes[1],
                            rpc[('car', 'time')] / 60,
                            "Cumul des courses en fonction de la durée",
                            "Durée parcourue en voiture (min)",
                            "Proportion des courses", 10)

    # Itinéraires en TC : comparaison des temps de parcours

    print("Nombre de courses dont l'alternative TC arrive + d'une heure en avance : ")
    print(sum(rpc['diff_arrival_time'].astype(
        'int') / 10e8 / 3600 > 1))

    _, axes = plt.subplots(2, 1)
    plot_hist(axes[0],
              rpc['ratio_transit_driving'][~rpc['ratio_transit_driving'].isna()
                                           ].sort_values(),
              "Ratio (durée course en voiture / durée trajet alternatif en TC)",
              "Ratio (durée course en voiture / durée trajet alternatif en TC)",
              "Proportion des courses", .1)

    plot_hist(axes[1],
              rpc['diff_transit_driving'][
        ~rpc['diff_transit_driving'].isna()].sort_values()/60,
        "Différence de durée entre l'itinéraire en TC et celui en voiture",
        "Différence en minutes (durée trajet alternatif en TC - durée course en voiture)",
        "Proportion des courses", 30)

    plt.show()


if __name__ == '__main__':
    main()
