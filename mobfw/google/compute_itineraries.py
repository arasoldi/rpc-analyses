import asyncio
import argparse
import sys
import aiohttp
from datetime import datetime, date, timedelta, timezone
import pandas as pd
from tqdm import tqdm


from ..config import N_THREADS
from ..constants import Mode
from .api import get_itinerary


OD_DAYTIME_COL_NAMES = [
    'start_lon', 'start_lat', 'end_lon', 'end_lat', 'query_dayofweek', 'query_time']


def astimezone(dt: datetime, tz, utcoffset=None):
    if utcoffset is None: return dt.astimezone(tz)
    if dt.tzinfo is tz: return dt
    utc = (dt - utcoffset).replace(tzinfo=tz)
    return tz.fromutc(utc)


def on_day(dayofweek, time):
    '''
    Revoie la prochaine date au jour `day`, à la même heure
    Pour éviter d'avoir des erreurs d'API de recherche d'itinéaires passés
    '''
    return datetime.combine(date(2022, 1, 17) + timedelta(days=dayofweek), time)


async def get_itinerary_metadata(  # pylint: disable=too-many-arguments
        start_lon, start_lat, end_lon, end_lat, end_dayofweek, end_time, arrive_by, session,
        semaphore):
    origin_destination_daytime = (start_lon, start_lat, end_lon, end_lat, end_dayofweek, end_time)
    expected_arrival_time = astimezone(on_day(end_dayofweek, end_time), tz=timezone.utc)
    path = await get_itinerary(
        start_lon, start_lat, end_lon, end_lat, mode=Mode.CAR,
        arrival_time=expected_arrival_time, session=session, semaphore=semaphore,
        arrive_by=arrive_by)
    return (*origin_destination_daytime, path["distance"]["value"], path["duration"]["value"])


async def get_itineraries(od_dts, arrive_by):
    '''
    Calcule les itinéraires en mode de transport simple (voiture, vélo, marche) à partir de besoins
    en mobilités
    '''
    semaphore = asyncio.Semaphore(N_THREADS)
    async with aiohttp.ClientSession() as session:
        tasks = [get_itinerary_metadata(*od_dt, arrive_by, session, semaphore) for od_dt in od_dts]

        api_responses = [
            await response for response in tqdm(asyncio.as_completed(tasks), total=len(tasks))]

        columns = [*OD_DAYTIME_COL_NAMES, "distance_car", "time_car"]

        itineraries = pd.DataFrame(api_responses, columns=columns)
        itineraries.set_index(OD_DAYTIME_COL_NAMES, inplace=True)
        itineraries["time_car"] = pd.TimedeltaIndex(itineraries["time_car"], unit='seconds')

        return itineraries


async def main():
    '''Main'''
    parser = argparse.ArgumentParser(
        "Calcule les itinéraires en voiture via l'API Google Direction.")
    parser.add_argument('path_in', help="Chemin vers demande en mobilité")
    parser.add_argument('path_out', help="Chemin vers fichier de sortie")
    parser.add_argument('-arrive_by', dest='arrive_by', action='store_true')
    parser.add_argument('-no-arrive_by', dest='arrive_by', action='store_false')
    parser.set_defaults(arrive_by=False)

    args = parser.parse_args()

    needs_path = args.path_in
    out_path = args.path_out

    print("Reading", needs_path)
    mobility_needs: pd.DataFrame = pd.read_pickle(needs_path)

    print("Extracting od_daytime")
    print("arrive_by:", args.arrive_by)
    if args.arrive_by:
        mobility_needs['query_datetime'] = mobility_needs["end_datetime"]
        index = ['start_lon', 'start_lat', 'end_lon', 'end_lat', 'end_dayofweek', 'end_time']
    else:
        mobility_needs['query_datetime'] = mobility_needs["start_datetime"]
        index = ['start_lon', 'start_lat', 'end_lon', 'end_lat', 'start_dayofweek', 'start_time']

    mobility_needs['query_dayofweek'] = mobility_needs["query_datetime"].dt.dayofweek
    mobility_needs['query_time'] = mobility_needs["query_datetime"].dt.time
    od_daytime = mobility_needs.value_counts(OD_DAYTIME_COL_NAMES).index

    print("Computing car itineraries")
    itineraries = await get_itineraries(od_daytime, args.arrive_by)
    itineraries.index.names = index

    print("Pickle to", out_path)
    itineraries.to_pickle(out_path)


if __name__ == "__main__":
    asyncio.run(main())
