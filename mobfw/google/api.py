"""
Everything related to Google is in this module.
"""

from enum import Enum

from ..constants import Mode

KEY = "AIzaSyAUKMPf0JEc6TUXGGApIt7IiS1hYZ6MlKE"


class GoogleMode(Enum):
    """
    Identifiants génériques de modes de transport utilisés dans l'API Google
    """
    DRIVING = 'driving'
    WALKING = 'walking'
    BYCYCLING = 'bicycling'
    TRANSIT = 'transit'


class GoogleTransitMode(Enum):
    """
    Identifiants génériques de modes de transport utilisés dans l'API Google
    """
    BUS = 'BUS'
    TRAM = 'TRAM'
    HEAVY_RAIL = 'HEAVY_RAIL'


BASE_URL = "https://maps.googleapis.com"


async def get_itinerary(start_lon, start_lat, end_lon, end_lat, mode, session, semaphore,
                        arrival_time=None, arrive_by=False) -> dict:
    """
    Récupère depuis l'API Google un itinéraire
    """
    if mode == Mode.TRANSIT:
        mode = GoogleMode.TRANSIT
    else:
        mode = GoogleMode.DRIVING

    params = {
        'origin': str(start_lat) + "," + str(start_lon),
        'destination': str(end_lat) + "," + str(end_lon),
        'mode': mode.value,
        'key': KEY,
    }

    if arrival_time is not None:
        params['departure_time'] = int(arrival_time.timestamp())

    async with semaphore:
        while True:
            try:
                response = await session.get(BASE_URL + "/maps/api/directions/json", params=params)
                response = await response.json()
                leg = response['routes'][0]['legs'][0]
            except:
                print("retry")
            else:
                break

    steps = leg['steps']

    return leg
