'''
Constantes
'''

from enum import Enum, auto


class Mode(Enum):
    """
    Représente les modes de transport pour différencier leurs caractéristiques
    """
    TRAM = auto()
    BUS_NGV = auto()
    BUS_DIESEL = auto()
    BUS_E = auto()
    BUS_HYB = auto()
    AUTOCAR = auto()
    CAR_PETROL = auto()
    CAR_E = auto()
    TER = auto()
    WALK = "walk"
    TRANSIT = "transit"
    CAR = "car"
    RIDESHARING = "ridesharing"
    MOTORBIKE = "motorbike"
    BIKE = "bike"
