'''
Calcule pour chaque besoin en mobilité des itinéraires dans différents modes de transport
'''

import asyncio
import argparse
from datetime import datetime, date, timedelta, timezone
import pandas as pd
import aiohttp

from tqdm import tqdm
from mobfw.beeline.calculate_distances import geodesic_distance

from mobfw.config import GTFS_ROUTE_TYPE_GHG, N_THREADS

from .constants import Mode

from .api import get_itinerary

OD_DAYTIME_COL_NAMES = [
    'start_lon', 'start_lat', 'end_lon', 'end_lat', 'query_dayofweek', 'query_time']
ROUTE_TYPES = [
    rt for rt in GTFS_ROUTE_TYPE_GHG if GTFS_ROUTE_TYPE_GHG[rt] is not None]


def astimezone(dt: datetime, tz, utcoffset=None):
    """Change timezone of a datetime"""
    if utcoffset is None:
        return dt.astimezone(tz)
    if dt.tzinfo is tz:
        return dt
    # Convert self to UTC, and attach the new time zone object.
    utc = (dt - utcoffset).replace(tzinfo=tz)
    # Convert from UTC to tz's local time.
    return tz.fromutc(utc)


def on_day(dayofweek, time):
    '''
    Revoie la prochaine date au jour `day`, à la même heure
    Pour éviter d'avoir des erreurs d'API de recherche d'itinéaires passés
    '''
    return datetime.combine(date(2022, 1, 3) + timedelta(days=dayofweek), time)


def line_length(line):
    """Retourne la longueur d'une ligne de points"""
    length = 0
    for i in range(len(line) - 1):
        length += geodesic_distance(line[i], line[i+1])
    return length


async def get_itinerary_metadata(  # pylint: disable=too-many-arguments
        start_lon, start_lat, end_lon, end_lat, end_dayofweek, end_time, arrive_by, session,
        semaphore):
    """Extrait les métadonnées utiles d'un itinéraire GraphHoppoer"""
    origin_destination_daytime = (
        start_lon, start_lat, end_lon, end_lat, end_dayofweek, end_time)
    expected_arrival_time = astimezone(
        on_day(end_dayofweek, end_time), tz=timezone.utc)
    path = await get_itinerary(
        start_lon, start_lat, end_lon, end_lat, mode=Mode.TRANSIT,
        arrival_time=expected_arrival_time, session=session, semaphore=semaphore,
        arrive_by=arrive_by)

    if not path:
        return (*origin_destination_daytime, 0, 0)

    walk_distance = 0
    pt_distances = {route_type: 0 for route_type in ROUTE_TYPES}
    n_walk = 0
    n_transit = 0
    for leg in path['legs']:
        if leg["type"] == "pt":
            pt_distances[leg["route_type"]
                         ] += line_length(leg["geometry"]["coordinates"])
            n_transit += 1
        elif leg["type"] == "walk":
            walk_distance += leg["distance"]
            n_walk += 1

    return (
        *origin_destination_daytime,
        walk_distance,
        *list(pt_distances.values()),
        path['time'],
        expected_arrival_time,
        path['legs'][-1]['arrival_time'],
        n_walk, n_transit)


async def get_itineraries(od_dts, arrive_by):
    '''
    Calcule les itinéraires en mode de transport simple (voiture, vélo, marche) à partir de besoins
    en mobilités
    '''
    semaphore = asyncio.Semaphore(N_THREADS)
    async with aiohttp.ClientSession() as session:
        tasks = [
            get_itinerary_metadata(*od_dt, arrive_by, session, semaphore) for od_dt in od_dts]

        api_responses = [
            await response for response in tqdm(asyncio.as_completed(tasks), total=len(tasks))]

        columns = [
            *OD_DAYTIME_COL_NAMES,
            "distance_" + Mode.WALK.value,
            *["distancept_" + str(rt)
              for rt in ROUTE_TYPES], "time_" + Mode.TRANSIT.value,
            "query_datetime_expected", "query_datetime_answered",
            "leg_count_walk", "leg_count_transit"]

        itineraries = pd.DataFrame(api_responses, columns=columns)
        itineraries.set_index(OD_DAYTIME_COL_NAMES, inplace=True)
        itineraries["time_" + Mode.TRANSIT.value] = pd.TimedeltaIndex(
            itineraries["time_" + Mode.TRANSIT.value], unit='ms')
        itineraries['query_datetime_expected'] = pd.to_datetime(
            itineraries['query_datetime_expected'])
        itineraries['query_datetime_answered'] = pd.to_datetime(
            itineraries['query_datetime_answered'])

        return itineraries


async def main():
    '''Main'''
    parser = argparse.ArgumentParser(
        "Calcule les itinéraires en TC via GraphHopper.")
    parser.add_argument('path_in', help="Chemin vers demande en mobilité")
    parser.add_argument('path_out', help="Chemin vers fichier de sortie")
    parser.add_argument('-arrive_by', dest='arrive_by', action='store_true')
    parser.add_argument('-no-arrive_by', dest='arrive_by', action='store_false')
    parser.set_defaults(arrive_by=True)

    args = parser.parse_args()

    needs_path = args.path_in
    out_path = args.path_out

    print("Reading", needs_path)
    mobility_needs: pd.DataFrame = pd.read_pickle(needs_path)

    print("Extracting od_daytime")
    print("arrive_by:", args.arrive_by)
    if args.arrive_by:
        mobility_needs['query_datetime'] = mobility_needs["end_datetime"]
        index = ['start_lon', 'start_lat', 'end_lon', 'end_lat', 'end_dayofweek', 'end_time']
    else:
        mobility_needs['query_datetime'] = mobility_needs["start_datetime"]
        index = ['start_lon', 'start_lat', 'end_lon', 'end_lat', 'start_dayofweek', 'start_time']

    mobility_needs['query_dayofweek'] = mobility_needs["query_datetime"].dt.dayofweek
    mobility_needs['query_time'] = mobility_needs["query_datetime"].dt.time
    od_daytime = mobility_needs.value_counts(OD_DAYTIME_COL_NAMES).index

    print("Computing transit itineraries")
    itineraries = await get_itineraries(od_daytime, args.arrive_by)
    itineraries.index.names = index

    print("Pickle to", out_path)
    itineraries.to_pickle(out_path)

if __name__ == '__main__':
    asyncio.run(main())
