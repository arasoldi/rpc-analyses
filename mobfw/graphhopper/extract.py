'''
Extrait les informations pertinentes des réponses API
'''
import sys

from datetime import datetime

import numpy as np
import pandas as pd
from dateutil import parser
#from pandarallel import pandarallel
from tqdm.auto import tqdm

from mobfw.utils import to_daytime

RELEVANT_INFORMATIONS = (
    "distance", "time", "departure_daytime", "arrival_daytime")


def extract_from_api_response(api_response, relevant_informations):
    '''
    Extrait les infos pertinentes depuis une réponse API de Graphhopper
    '''
    try:
        path = api_response['paths'][0]
    except KeyError:
        return pd.Series([None] * len(relevant_informations), index=relevant_informations)

    infos = [
        path['distance'],
        path['time'],
    ]

    try:
        infos.append(to_daytime(parser.parse(
            path['legs'][0]['departure_time'])))
        infos.append(to_daytime(parser.parse(
            path['legs'][-1]['arrival_time'])))
        return pd.Series(infos, index=relevant_informations)
    except IndexError:
        pass

    return pd.Series(infos, index=relevant_informations)


def extract_from_api_response_column(legs, transit=False):
    '''
    Used in apply function
    '''
    relevant_informations = RELEVANT_INFORMATIONS if transit else RELEVANT_INFORMATIONS[:2]

    if isinstance(legs, dict):
        return extract_from_api_response(legs, relevant_informations)

    if isinstance(legs, list):
        if len(legs) == 0:
            return pd.Series(np.zeros(len(relevant_informations)))

        leg_infos = extract_from_api_response(legs[0], relevant_informations)
        journey_infos = leg_infos
        for leg in legs[1:]:
            journey_infos += extract_from_api_response(
                leg, relevant_informations)
        return journey_infos

    print("Error, unknown type")
    print(legs)
    return None


def main(args):
    '''
    Main
    '''
    if len(args) < 3:
        print("Pas assez de paramètres. Indiquer fichier d'entrée et fichier de sortie.")
        return
    #print(datetime.now(), "Initialisation de pandarallel")
    #pandarallel.initialize(nb_workers=4, progress_bar=True, use_memory_fs=False)
    print(datetime.now(), "Lecture du fichier :", args[1])
    itineraries_df = pd.read_pickle(args[1])
    print(datetime.now(), "Fin de lecture du fichier")
    infos = {}
    tqdm.pandas(desc="Extraction")
    for mode_name in itineraries_df.columns.get_level_values(0):
        if "api_response" in itineraries_df[mode_name].columns:
            print(datetime.now(), "Début d'extraction :", mode_name)
            infos_mode = itineraries_df[(mode_name, "api_response")].progress_apply(
                extract_from_api_response_column,
                args=[mode_name == "transit"])
            infos[mode_name] = infos_mode
            print(datetime.now(), "Fin d'extraction :", mode_name)

    print(datetime.now(), "Concaténation des données")
    pd.concat(infos.values(), axis=1, keys=infos.keys()).to_pickle(args[2])
    print(datetime.now(), "Fin")


if __name__ == '__main__':
    main(sys.argv)
