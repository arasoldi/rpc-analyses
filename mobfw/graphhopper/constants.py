'''
Constantes relatives à GraphHopper
'''

from ..constants import Mode

MODE_MAP = {
    Mode.BIKE: 'bike',
    Mode.MOTORBIKE: 'motorcycle',
    Mode.CAR: 'car',
    Mode.WALK: 'foot',
    Mode.TRANSIT: 'pt'
}
