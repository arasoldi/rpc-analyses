'''
Calcule pour chaque besoin en mobilité des itinéraires en TC
'''

import sys
import asyncio
from datetime import datetime, date, timedelta
import pandas as pd
import aiohttp

from tqdm import tqdm

from .api import get_itinerary
from .constants import Mode


####################################################################################################
#                                        TRANSIT                                                   #
####################################################################################################


def on_day(daytime):
    '''
    Revoie la prochaine date au jour `day`, à la même heure
    Pour éviter d'avoir des erreurs d'API de recherche d'itinéaires passés
    '''
    try:
        weekday, timetz = daytime
    except TypeError as error:
        print(daytime)
        raise Exception("daytime is not correct, see value above") from error
    return datetime.combine(
        date.today() + timedelta(days=(weekday - date.today().weekday() + 7) % 14),
        timetz, tzinfo=timetz.tzinfo)


async def query_transit_itinerary(journey, session, semaphore):
    '''
    Query the direct transit itinerary between pt1 and pt2.
    '''
    start_pt = journey['start_pt']
    end_pt = journey['end_pt']
    journey_end_daytime = journey['journey_end_daytime']

    if start_pt == end_pt:
        return pd.Series({'api_response': 0}, name=(start_pt, end_pt, journey_end_daytime))

    arrival_datetime = on_day(journey_end_daytime)
    response = await get_itinerary(
        start_pt, end_pt, Mode.TRANSIT, arrival_datetime, session, semaphore)

    try:
        return pd.Series({'api_response': response},
                         name=(start_pt, end_pt, journey_end_daytime))

    except IndexError:
        return pd.Series({'api_response': None}, name=(start_pt, end_pt, journey_end_daytime))


async def query_transit_itineraries(mobility_needs: pd.DataFrame):
    '''
    Compute itineraries for transit from mobility needs
    '''
    journeys = mobility_needs.value_counts(
        ['start_pt', 'end_pt', 'journey_end_daytime']).index.to_frame()

    async with aiohttp.ClientSession() as session:
        semaphore = asyncio.Semaphore(8)
        tasks = [query_transit_itinerary(journey[1], session, semaphore=semaphore)
                 for journey in journeys.iterrows()]
        api_responses = [await f for f in tqdm(asyncio.as_completed(tasks), total=len(tasks))]
        transit_itineraries = pd.DataFrame(api_responses)

    return mobility_needs.join(
        transit_itineraries[['api_response']],
        on=['start_pt', 'end_pt', 'journey_end_daytime'])[['api_response']]


async def main(args):
    '''
    Main
    '''
    mobility_needs = pd.read_pickle(args[1])

    print("Computing transit itineraries")
    pd.concat(
        [await query_transit_itineraries(mobility_needs)], keys=[Mode.TRANSIT.value], axis=1
    ).to_pickle(args[2])

if __name__ == '__main__':
    asyncio.run(main(sys.argv))
