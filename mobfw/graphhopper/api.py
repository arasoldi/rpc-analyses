'''
Appels API à Graphhopper
'''

import asyncio
import aiohttp

from .constants import MODE_MAP
from ..config import GRAPHHOPPER_HOST as HOST, GRAPHHOPPER_PORT as PORT


async def get_itinerary(start_lon, start_lat, end_lon, end_lat, mode,
                        session: aiohttp.ClientSession, semaphore,
                        arrival_time=None, arrive_by=False) -> dict:
    """Call the API to query the itinerary between pt1 and pt2 with the mode `mode`,
    using GraphHopper (local)
    arrival_time MUST BE IN UTC
    """
    pt1 = str(start_lat) + "," + str(start_lon)
    pt2 = str(end_lat) + "," + str(end_lon)
    params = {'point': [pt1, pt2],
              'profile': MODE_MAP[mode], 'calc_points': 'false'}

    if arrival_time is not None:
        params['pt.arrive_by'] = "true" if arrive_by else "false"
        params['pt.earliest_departure_time'] = arrival_time.isoformat().replace(
            '+00:00', 'Z')

    while True:
        try:
            async with semaphore:
                query_url = "http://" + HOST + ":" + str(PORT) + "/route"
                response = await session.get(query_url, params=params)
        except asyncio.TimeoutError:
            print("TimeoutError, retrying...")
        except aiohttp.ServerDisconnectedError:
            print("ServerDisconnectedError, retrying...")
        else:
            break

    response_json = await response.json()

    if 'paths' in response_json:
        if response_json['paths']:
            return response_json['paths'][0]  # Only take the first result
        raise Exception("Cannot find path from " + pt1 + " to " + pt2 + ". Try: " + response.url)
    if 'details' in response_json and response_json['details'] \
            == 'com.graphhopper.util.exceptions.ConnectionNotFoundException':
        return None
    # encountered unknown error
    print('from', pt1, 'to', pt2)
    print(response)

    raise Exception("No path found " + str(response_json) + " Try : " + str(response.url))
