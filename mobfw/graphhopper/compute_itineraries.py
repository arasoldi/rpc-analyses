'''
Calcule pour chaque besoin en mobilité des itinéraires dans différents modes de transport
'''

import sys
import asyncio
import pandas as pd
import aiohttp

from tqdm import tqdm

from mobfw.config import N_THREADS

from .constants import Mode

from .api import get_itinerary

OD_COL_NAMES = ['start_lon', 'start_lat', 'end_lon', 'end_lat']


async def get_itinerary_metadata(start_lon, start_lat, end_lon, end_lat, mode, session, semaphore):
    """Extrait les métadonnées utiles d'un itinéraire GraphHoppoer"""
    origin_destination = (start_lon, start_lat, end_lon, end_lat)
    path = await get_itinerary(*origin_destination, mode, session, semaphore)
    if path is None:
        return (*origin_destination, 0, 0)
    return (*origin_destination, path['distance'], path['time'])


async def get_itineraries(unique_ods, mode: Mode):
    '''
    Calcule les itinéraires en mode de transport simple (voiture, vélo, marche) à partir de besoins
    en mobilités
    '''
    semaphore = asyncio.Semaphore(N_THREADS)
    async with aiohttp.ClientSession() as session:
        tasks = [
            get_itinerary_metadata(*od, mode, session, semaphore) for od in unique_ods]

        api_responses = [
            await response for response in tqdm(asyncio.as_completed(tasks), total=len(tasks))]

        columns = [*OD_COL_NAMES, "distance_" +
                   mode.value, "time_" + mode.value]
        itineraries = pd.DataFrame(api_responses, columns=columns)
        itineraries.set_index(OD_COL_NAMES, inplace=True)
        itineraries["time_" + mode.value] = pd.TimedeltaIndex(
            itineraries["time_" + mode.value], unit='ms')

        return itineraries


async def main(args):
    '''Main'''
    path_in = args[1]
    path_out = args[2]
    modes = [Mode(mode_name) for mode_name in args[3].split(',')]

    print("Reading", path_in)
    mobility_needs: pd.DataFrame = pd.read_pickle(path_in)
    origin_destinations = mobility_needs.value_counts(OD_COL_NAMES).index

    print("Computing simple modes itineraries:", modes)
    for mode in modes:
        itineraries = await get_itineraries(origin_destinations, mode)
        path_out_mode = path_out + "." + mode.value
        print("Extracting to", path_out_mode)
        itineraries.to_pickle(path_out_mode)


if __name__ == '__main__':
    asyncio.run(main(sys.argv))
