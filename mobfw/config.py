'''
Configuration
'''

import numpy as np
from mobfw.constants import Mode
from uncertainties import *

GRAPHHOPPER_HOST = 'localhost'
GRAPHHOPPER_PORT = 8989

N_THREADS = 4

MODALITIES = [
    Mode.CAR,
    Mode.RIDESHARING,
    Mode.MOTORBIKE,
    Mode.BIKE,
    Mode.WALK,
    Mode.TRANSIT
]

BETA = np.array([
    -4.2660,  # Cs_VP_partagée
    -3.1565,  # Cs_moto
    -2.5986,  # Cs_vélo
    -0.4273,  # Cs_marche
    -1.0308,  # TC
    -0.0013,  # Coût
    -0.0028  # Temps : Normalisation pour mettre en secondes
])

SPEED = {  # km / h
    Mode.CAR: 42,
    Mode.RIDESHARING: 42,
    Mode.MOTORBIKE: 42,
    Mode.BIKE: 14,
    Mode.WALK: 4.7,
    Mode.TRANSIT: 33
}

SPEED_M_PER_S = dict(zip(  # normalisée en m/s
    SPEED.keys(),
    [v/3.6 for v in SPEED.values()]))

COST = {  # centimes par km
    Mode.CAR: 53,
    Mode.RIDESHARING: 27,
    Mode.MOTORBIKE: 33,
    Mode.BIKE: 12,
    Mode.WALK: 17,
    "TC Île-de-France": 11,
    Mode.TRANSIT: 13,
    "TER": 6
}

COST_PER_M = dict(zip(  # centimes par m
    COST.keys(),
    [v/1000 for v in COST.values()]))

TRAVEL_SPECS = ['cost', 'time']

ADEME_FACTORS = { # kgCO2e/km(/passager)
    "Voiture - Motorisation moyenne - 2018": ufloat(0.192, .6*.192),
    "Cyclomoteur - Mixte - 2018": 0.0644,
    "Autobus moyen - Agglomération moins de 100 000 habitants": ufloat(0.146, .6*.146),
    "Autobus moyen - Agglomération de 100 000 à 250 000 habitants": ufloat(0.137, .6*.137),
    "Autobus moyen - Agglomération de plus de 250 000 habitants": ufloat(0.129, .6*.129),
    "Train grandes lignes - 2019": 5.29E-3,
    "TGV - 2019": 1.73E-3,
    "TER - 2019 - Traction moyenne": ufloat(0.0248, .6*0.0248),
    "Métro - 2019 - Ile de France": 2.50E-3,
    "RER et transilien - 2019 - Ile de France": 4.10E-3,
    "Tramway - 2019 - Ile de France": 2.20E-3,
    "Métro, tramway, trolleybus - 2018 - Agglomération > à 250 000 habitants": ufloat(2.98E-3,.6*2.98E-3),
    "Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants": ufloat(4.72E-3,.6*4.72E-3),
    "Ferry - de jour (passagers)": 0.979, # valeur obsolète
    "Télécabine - 2010": 0.0295,
    "Autocar - Gazole": ufloat(0.0295,.6*0.0295)
}

# pylint: disable=line-too-long
GTFS_ROUTE_TYPE_GHG = {
    0: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], #tramway ou métro léger. Tout système de métro léger ou circulant sur la chaussée dans une zone métropolitaine.
    1: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], #métro. Tout système ferroviaire souterrain circulant au sein d'une zone métropolitaine.
    2: ADEME_FACTORS["Train grandes lignes - 2019"], #train. Utilisé pour les trajets interurbains ou longue distance.
    3: ADEME_FACTORS['Autobus moyen - Agglomération moins de 100 000 habitants'], #bus. Utilisé pour les lignes de bus courte et longue distance.
    4: ADEME_FACTORS["Ferry - de jour (passagers)"], #ferry. Utilisé pour le service de bateaux courte et longue distance.
    5: None, #tramway à traction par câble. Utilisé pour les systèmes de tramways au niveau de la chaussée dans lesquels le câble passe sous le véhicule, comme c'est le cas à San Francisco.
    6: ADEME_FACTORS["Télécabine - 2010"], #téléphérique. Service de transport par câble où les cabines, voitures, télécabines ou sièges sont suspendus à l'aide d'un ou de plusieurs câbles.
    7: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], #funiculaire. Tout système ferroviaire conçu pour les pentes raides.
    11: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], #trolleybus. Autobus électrique alimenté par des lignes aériennes de contact.
    12: None, #monorail. Service de chemin de fer roulant sur une voie constituée d'un rail ou d'une poutre unique.
    100: ADEME_FACTORS["Train grandes lignes - 2019"], # Service de chemin de fer
    101: ADEME_FACTORS["TGV - 2019"], # Service de chemin de fer à grande vitesse
    102: ADEME_FACTORS["Train grandes lignes - 2019"], # Grandes lignes de chemin de fer
    103: ADEME_FACTORS["TER - 2019 - Traction moyenne"], # Service de chemin de fer transrégional
    104: None, # Service de transport de voitures par chemin de fer
    105: ADEME_FACTORS["Train grandes lignes - 2019"], # Service de chemin de fer à couchettes
    106: ADEME_FACTORS["TER - 2019 - Traction moyenne"], # Service de chemin de fer régional
    107: ADEME_FACTORS["Train grandes lignes - 2019"], # Service de chemin de fer touristique
    108: None, # Navette ferroviaire (au sein d'un complexe)
    109: ADEME_FACTORS["RER et transilien - 2019 - Ile de France"], # Chemin de fer de banlieue
    110: None, # Service de chemin de fer de substitution
    111: None, # Service de chemin de fer spécial
    112: None, # Service de transport de camions par chemin de fer
    113: ADEME_FACTORS["Train grandes lignes - 2019"], # Service de chemin de fer (toute catégorie)
    114: ADEME_FACTORS["TER - 2019 - Traction moyenne"], # Service de chemin de fer transnational
    115: None, # Service de transport de véhicules par chemin de fer
    116: None, # Chemin de fer à crémaillère
    117: None, # Service de chemin de fer supplémentaire
    200: ADEME_FACTORS["Autocar - Gazole"], # Service de cars
    201: ADEME_FACTORS["Autocar - Gazole"], # Service de cars internationaux
    202: ADEME_FACTORS["Autocar - Gazole"], # Service de cars nationaux
    203: ADEME_FACTORS["Autocar - Gazole"], # Service de navette par cars
    204: ADEME_FACTORS["Autocar - Gazole"], # Service de cars régionaux
    205: ADEME_FACTORS["Autocar - Gazole"], # Service de cars spéciaux
    206: ADEME_FACTORS["Autocar - Gazole"], # Service de visite touristique en car
    207: ADEME_FACTORS["Autocar - Gazole"], # Service de cars touristiques
    208: ADEME_FACTORS["Autocar - Gazole"], # Service de cars de banlieue
    209: ADEME_FACTORS["Autocar - Gazole"], # Service de cars (toute catégorie)
    400: ADEME_FACTORS["RER et transilien - 2019 - Ile de France"], # Service de chemin de fer urbain
    401: ADEME_FACTORS["RER et transilien - 2019 - Ile de France"], # Service de chemin de fer métropolitain
    402: None, # Service de transport souterrain
    403: ADEME_FACTORS["RER et transilien - 2019 - Ile de France"], # Service de chemin de fer urbain
    404: ADEME_FACTORS["RER et transilien - 2019 - Ile de France"], # Service de chemin de fer urbain (toute catégorie)
    405: None, # Monorail
    700: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus
    701: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus régional
    702: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus express
    703: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus desservant des arrêts
    704: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus local
    705: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus nocturne
    706: ADEME_FACTORS["Autocar - Gazole"], # Service de car postal
    707: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus pour passagers présentant des besoins spéciaux
    708: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus pour personnes à mobilité réduite
    709: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus pour personnes à mobilité réduite (déclarées comme telles)
    710: None, # Autobus panoramique
    711: None, # Service de navette par autobus
    712: ADEME_FACTORS["Autocar - Gazole"], # Bus scolaire
    713: ADEME_FACTORS["Autocar - Gazole"], # Bus scolaire et à usage public
    714: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service de substitution par autobus (remplacement d'un service de chemin de fer)
    715: None, # Service de transport par autobus à la demande
    716: ADEME_FACTORS["Autobus moyen - Agglomération moins de 100 000 habitants"], # Service d'autobus (toute catégorie)
    800: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de trolleybus
    900: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway
    901: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway urbain
    902: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway local
    903: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway régional
    904: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway touristique
    905: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de navette par tramway
    906: ADEME_FACTORS["Métro, tramway, trolleybus - 2018 - Agglomération de 100 000 à 250 000 habitants"], # Service de tramway (toute catégorie)
    1000: None, # Service de transport fluvial
    1100: None, # Service de transport aérien
    1200: None, # Service de ferry
    1300: ADEME_FACTORS["Télécabine - 2010"], # Service de téléphérique
    1400: None, # Service de funiculaire
    1500: None, # Service de taxis
    1501: None, # Service de taxis communaux
    1502: None, # Service de taxi fluvial
    1503: None, # Service de taxis en gare de chemin de fer
    1504: None, # Service de vélotaxi
    1505: None, # Service de taxis réglementé
    1506: ADEME_FACTORS["Voiture - Motorisation moyenne - 2018"], # Service de location de véhicules particuliers
    1507: None, # Service de taxis (toute catégorie)
    1700: None, # Services divers
    1702: None, # Calèche
}
