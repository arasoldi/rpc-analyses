'''
Module de prédiction par modèle Logit
'''

import sys
import numpy as np
import pandas as pd

from ..config import BETA, MODALITIES, TRAVEL_SPECS


def get_spec(specs, mode, spec_name):
    """Renvoie la caractéristique d'un trajet"""
    return specs[(mode.value, spec_name)]


def dummies(count, index):
    """Retourne des dummy variables et met à 1 la ième"""
    dummy = np.zeros(count - 1)
    if index == 0:
        return dummy
    dummy[0] = 1
    return np.roll(dummy, index - 1)


def get_specs(individual_offer, modes):
    """Retourne les specificités du trajet utiles pour le calcul des utilités dans chaque mode
    pour un individu"""
    return [[get_spec(individual_offer, m, spec) for spec in TRAVEL_SPECS] for m in modes]


def x_i(specs):
    """Crée chaque vecteur x_ij à partir des paramètres des trajets pour chaque modalité
    pour un individu"""
    return [[*dummies(len(specs), i), *m_specs] for i, m_specs in enumerate(specs)]


def calculate_utilities(specs, beta):
    """Calcule les utilités à partir des paramètres du trajet pour un individu"""
    return np.sum(x_i(specs)*beta, axis=1)


def calculate_probabilities(individual_utilities):
    """Calcule les probabilités Logit d'un individu pour chaque modalité à partir des utilités"""
    odds = np.exp(individual_utilities)
    return pd.Series(odds / np.sum(odds))


def main(args):
    """Main"""
    offer = pd.concat([pd.read_pickle(p) for p in args[1].split(',')], axis=1)

    utilities = offer.apply(
        lambda individual_offer, modes=MODALITIES, beta=BETA:
        calculate_utilities(get_specs(individual_offer, modes), beta), axis=1)

    probabilities = utilities.apply(calculate_probabilities)\
                             .set_axis([m.value for m in MODALITIES], axis=1)

    print(probabilities.mean())


if __name__ == '__main__':
    main(sys.argv)
