'''
Calcule les prix des trajets
'''
import sys
import pandas as pd
from ..constants import Mode
from ..config import COST


def calculate_cost(mode, distance):
    '''
    Calcule le prix d'un trajet selon les coefficients MODE_COST_PER_KM
    '''
    return COST[mode] # coût constant
    return COST[mode] * distance # coût proportionnel à la distance


def main(args):
    """Main    """
    infos = pd.read_pickle(args[1])
    costs = pd.DataFrame()
    for mode_name in infos.columns.levels[0]:
        costs[(mode_name, 'cost')] = infos[(mode_name, 'distance')]\
            .apply(lambda d, mode_name=mode_name: calculate_cost(Mode(mode_name), d))

    print(costs.mean())
    costs.to_pickle(args[2])


if __name__ == '__main__':
    main(sys.argv)
