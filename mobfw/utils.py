"""Fonctions utilitaire"""


def to_daytime(datetime):
    """Convertit un objet datetime en couple (jour de la semaine, heure)"""
    return (datetime.weekday(), datetime.timetz())
