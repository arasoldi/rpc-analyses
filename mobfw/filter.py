'''
Filter mobility needs on space and time
'''

from datetime import datetime
import argparse
from dateutil.parser import parse

import numpy as np
import pandas

import geopandas as gpd


def filter_departements(mobility_needs, dep_no):
    '''
    Retourne des données uniquement des départements dont les numéros sont listés dans dep_no
    '''

    if not dep_no:
        return mobility_needs

    return mobility_needs[
        np.logical_or.reduce(
            list(map(lambda d: mobility_needs['journey_start_insee'].str.startswith(
                d, na=False), dep_no))) & np.logical_or.reduce(
            list(map(lambda d: mobility_needs['journey_end_insee'].str.startswith(
                d, na=False), dep_no)))
    ]


def filter_date(mobility_needs, date_after):
    '''
    Retourne les données datées après `date_after`
    '''
    return mobility_needs[
        mobility_needs['journey_start_datetime'] > date_after
    ]


def get_departement_bounds(data_folder, dep_num):
    '''
    Gets bounds for a group of departements
    '''
    with open(data_folder + "/departements-version-simplifiee.geojson") as file:
        gdf = gpd.read_file(file).set_index('code')
    dep_bounds = gdf.loc[dep_num]['geometry'].bounds
    min_lon, min_lat, _, _ = dep_bounds.min()
    _, _, max_lon, max_lat = dep_bounds.max()
    return [min_lon - .05, max_lon + .05, min_lat - .05, max_lat + .05]


AUVERGNE_RHONE_ALPES = ['38', '73', '05', '26', '07', '42', '69', '01']
ISERE = ['38']


def main():
    '''
    Main
    '''
    parser = argparse.ArgumentParser(
        "Filter mobility data.")
    parser.add_argument('path_in', help="Mobility data")
    parser.add_argument('path_out', help="Output path for filtered data")
    parser.add_argument('-date_after', '-A',
                        help='Keep only demand after the mentioned date.')
    parser.add_argument('-departements', '-D', help='Filter departments (departments numbers)')

    args = parser.parse_args()

    mobility_needs = pandas.read_pickle(args.path_in)

    date_after = parse(args.date_after).replace(tzinfo=datetime.now().astimezone().tzinfo)

    if args.departements:
        departements = args.departements.split(',')
    else:
        departements = None

    filter_departements(
        filter_date(
            mobility_needs, date_after),
        departements
    ).to_pickle(args.path_out)


if __name__ == '__main__':
    main()
