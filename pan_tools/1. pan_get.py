"""Download GTFS files from Plan d'Accès National"""

#!/usr/bin/env python
# coding: utf-8
# In[]
import itertools
import json
import os
import socket

import cgi
import shutil
import urllib.request
import urllib.parse
import contextlib

DATASET_BASE_URL = "https://www.data.gouv.fr/fr/datasets/r/"
PAN_PATH = "../data/pan"
GTFS_PATH = os.path.join(PAN_PATH, "gtfs")


def get_urls_from_csv(csv):
    """Parse CSV and get download URLs from there."""
    lines = csv.splitlines()
    for line in lines:
        for cell in line.split(";"):
            if cell.startswith("http") or cell.startswith("ftp"):
                yield cell.strip()
                break


def download_resource(url, datagouv_id):
    """Download resource"""
    with contextlib.closing(urllib.request.urlopen(url)) as req:
        with contextlib.closing(urllib.request.urlopen(req.url)) as req_final:
            _, params = cgi.parse_header(
                req_final.headers.get('Content-Disposition', ''))
            try:
                filename = params['filename']
            except KeyError:
                filename = os.path.basename(
                    urllib.parse.urlparse(req_final.url).path).lower()

            content_type = req_final.info().get_content_type()
            if filename.endswith("zip") or content_type in [
                    "application/zip", "application/octet-stream"]:
                newfilepath = os.path.join(
                    GTFS_PATH, datagouv_id + "_" + filename)
                if not filename.endswith(".zip"):
                    newfilepath += ".zip"
                print("zip", end=" ")
                if os.path.isfile(newfilepath):
                    print("File exists, ignoring")
                    return
                with open(newfilepath, 'wb') as file:
                    shutil.copyfileobj(req_final, file)
                    print("OK")
            elif filename.endswith("csv") or content_type == "application/csv":
                print("csv")
                with contextlib.closing(urllib.request.urlopen(req.url)) as req_final:
                    csv = req_final.read().decode(req_final.headers.get_content_charset())
                    for url_from_csv in get_urls_from_csv(csv):
                        print("extra url", end=" ")
                        download_resource(url_from_csv, datagouv_id)
            else:
                print("Error: unknown type", filename,
                      content_type, datagouv_id)
                return


def check_file_exists(datagouv_id, current_files):
    """Check if a GTFS file is already existing."""
    for file in current_files:
        if file.startswith(datagouv_id):
            return True
    return False


# In[3]:
datasets = json.loads(urllib.request.urlopen(
    "https://transport.data.gouv.fr/api/datasets").read())

gtfs_resources = filter(
    lambda resource: resource['format'] == 'GTFS',
    itertools.chain(*map(lambda dataset: dataset['resources'], datasets)))
datagouv_ids = set(map(lambda r: r['datagouv_id'], gtfs_resources))
nb_resources = len(datagouv_ids)
print("Nombre de ressources :", nb_resources)

# In[5]:
current_gtfs_files = [f for f in os.listdir(
    GTFS_PATH) if os.path.isfile(os.path.join(GTFS_PATH, f))]

for i, d_id in enumerate(datagouv_ids):
    print(i, '/', nb_resources, end=" ")
    if check_file_exists(d_id, current_gtfs_files):
        print("file exists")
        continue
    print(DATASET_BASE_URL + str(d_id), end=" ")
    try:
        download_resource(DATASET_BASE_URL + d_id, d_id)
    except urllib.request.HTTPError as error:
        print("Error:", error)
    except socket.timeout as error:
        print("Time out", error)
    except urllib.request.URLError as error:
        print("Error:", error)

# %%
