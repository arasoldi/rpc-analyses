'''
Vérifie la validité de tous les fichiers GTFS du Plan d'Accès National
Pour le lancer (répertoire parent) :
pypy3 snippets/pan_validate.py ../data/pan/gtfs valid.txt err.txt
'''

import sys
import os
from optparse import Values # pylint: disable=deprecated-module
import pandas as pd
sys.path.append('transitfeed')
print(os.path.abspath(sys.path[-1]))
import feedvalidator # type: ignore # pylint: disable=import-error, wrong-import-position
import transitfeed # pylint: disable=import-error, wrong-import-position

def absolute_file_paths(directory):
    """Get absolute file path of files in directory"""
    for dirpath, _, filenames in os.walk(directory):
        for filename in filenames:
            yield os.path.abspath(os.path.join(dirpath, filename))


def _check_feed(feed, valid_output_file, err_output_file):
    options = Values({
        'manual_entry': True, 'output': 'CONSOLE', 'performance': None, 'memory_db': True,
        'check_duplicate_trips': False, 'limit_per_type': 5, 'latest_version': '',
        'service_gap_interval': 13, 'extension': None, 'error_types_ignore_list': None})
    print(feed)
    accumulator = feedvalidator.HTMLCountingProblemAccumulator(options.limit_per_type) # pylint: disable=no-member
    loader = transitfeed.Loader(
        feed, problems=transitfeed.ProblemReporter(accumulator), extra_validation=False,
        memory_db=options.memory_db, check_duplicate_trips=options.check_duplicate_trips,
        gtfs_factory=transitfeed.GetGtfsFactory())
    loader.Load().Validate(
        service_gap_interval=options.service_gap_interval, validate_children=False)

    if accumulator.ErrorCount():
        err_output_file.write(feed + ',')
    else:
        valid_output_file.write(feed + ',')

def main(args):
    """main"""
    gtfs_path = os.path.abspath(args[1])
    valid_output_path = args[2]
    err_output_path = args[3]
    
    if len(args) > 4:
        gtfs_files = pd.read_csv(args[4])["filename"].apply(lambda f: os.path.join(gtfs_path, f))
    else:
        gtfs_files = filter(lambda filename: filename.endswith(".zip"), absolute_file_paths(gtfs_path))

    with open(valid_output_path, 'w') as valid_output_file,\
         open(err_output_path, 'w')   as err_output_file:
        for feed in gtfs_files:
            _check_feed(feed, valid_output_file, err_output_file)


if __name__ == '__main__':
    main(sys.argv)
