"""Filtre les fichiers GTFS pour en garder uniquement ceux qui sont dans la zone indiquée."""

#!/usr/bin/env python
# coding: utf-8

# In[1]:
import os
import json
import pandas as pd

DATA_PATH = "../../data"
PAN_PATH = os.path.join(DATA_PATH, "pan")
GTFS_PATH = os.path.join(PAN_PATH, "gtfs")

# In[]

with open(os.path.join(GTFS_PATH, "datasets.json")) as file:
    datasets = pd.json_normalize(json.load(file))

# %%
datasets = datasets.explode("resources")
datasets.reset_index(drop=True, inplace=True)
# %%
resources = datasets["resources"].apply(pd.Series)
resources.dropna(axis=1, how='all', inplace=True)
# %%
resources = datasets.join(resources.add_prefix("resource."))
resources_gtfs = resources[resources["resource.format"] == "GTFS"]

# %%
file_names = os.listdir(GTFS_PATH)
file_datagouv_ids = pd.Series({n[:36]: n for n in file_names}, name="filename")
# %%
# Agglomération de Grenoble
# covered_area_names = [
#     "Auvergne-Rhône-Alpes",
#     "SMM de l’Aire Grenobloise",
#     "Syndicat Mixte des 4 Communautés de Communes",
#     "CA Porte de l’Isère",
#     "Grand Chambéry",
#     "France"
# ]
# Agglomération de Nantes
covered_area_names = [
    "Nantes",
    "Nantes Métropole",
    "Pays de la Loire",
    "France"
]
#%%
covered_gtfs = resources_gtfs[resources_gtfs["covered_area.name"].isin(covered_area_names)]
covered_files = pd.merge(covered_gtfs, file_datagouv_ids, left_on="resource.datagouv_id", right_index=True)["filename"]
covered_files.to_csv("covered_gtfs_files.csv")
# %%
",".join(covered_files.values)